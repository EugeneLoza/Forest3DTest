program castleworlddemo;

{$R+}{$Q+}

uses
  SysUtils,
  Classes,
  Castle_base,
  Castle_Window,
  CastleWindow,
  CastleCameras,
  x3dload,
  CastleVectors,
  x3dnodes,
  CastleScene,
  Castlescenecore,
  CastleControls,
  CastleLog,
  Castleworld;

{$DEFINE UseWorld}

const
  objx = 19;
  objy = 19;

var
  MainCamera: TWalkCamera;
  Objects: array[0..objx, 0..objy] of TX3DRootNode;

    {$IFDEF UseWorld}
  World: TCastleWorld;

    {$ENDIF}

  procedure LoadObjects;
  var
    Raptor: TX3DRootNode;
    tmpTransformNode: TTransformNode;
    ix, iy: integer;
    {$IFNDEF UseWorld}
    tmpScene: TCastleScene;
    {$ENDIF}
  begin
    Raptor := load3d('raptor_1.x3d');
 {$IFDEF UseWorld}
    World := TCastleWorld.Create(Window);
 {$ENDIF}
    for ix := 0 to objx do
      for iy := 0 to objy do
      begin
        tmpTransformNode := TTransformNode.Create;
        tmpTransformNode.FdChildren.Add(Raptor);
        tmpTransformNode.Translation := Vector3(ix * 16 + 6, 0, -iy * 16);
        tmpTransformNode.Scale := Vector3(0.5 + Random, 0.5 + Random, 0.5 + Random);
        tmpTransformNode.Rotation := Vector4(0, 1, 0, Random * 2 * Pi);
        Objects[ix, iy] := TX3DRootNode.Create;
        Objects[ix, iy].FdChildren.Add(TmpTransformNode);

      {$IFDEF UseWorld}
        World.Add(Objects[ix, iy]);
      {$ELSE}
        tmpScene := TCastleScene.Create(Window);
        TmpScene.Load(Objects[ix, iy], True);
        Window.SceneManager.Items.Add(tmpScene);
      {$ENDIF}
      end;
 {$IFDEF UseWorld}
    World.ChunkNSlice;
 {$ENDIF}
  end;


  procedure doWindowRender(Container: TUIContainer);
  begin
    UIFont.Print(10, 10, Vector4(1, 1, 1, 1),
      Format('FPS : %f (real : %f). Shapes : %d / %d',
      [Window.Fps.OnlyRenderFps, Window.Fps.RealFps,
      Window.SceneManager.Statistics.ShapesRendered,
      Window.SceneManager.Statistics.ShapesVisible]));

  {$IFDEF UseWorld}
    World.Manage;
{$ENDIF}
  end;

begin
  Randomize;
  initializeLog;

  {I'm not sure, how World.Manage would work if limit FPS is not zero...}
  application.LimitFPS := 0;

  Window := TCastleWindow.Create(Application);
  Window.onRender := @doWindowRender;
  MainCamera := TWalkCamera.Create(Window);
  MainCamera.MouseLook := True;
  MainCamera.MoveSpeed := 20;
  Window.SceneManager.Camera := MainCamera;

  Window.Open;
  LoadObjects;
  Application.Run;

end.
