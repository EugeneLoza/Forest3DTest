# Back story

There is a true story of a letter to gamedev firm that has became canonical and gave birth to tons of memes. The first part of original is:

Здраствуйте. Я, Кирилл. Хотел бы чтобы вы сделали игру, 3Д-экшон суть такова… Пользователь может играть лесными эльфами, охраной дворца и злодеем. И если пользователь играет эльфами то эльфы в лесу, домики деревяные набигают солдаты дворца и злодеи. Можно грабить корованы… И эльфу раз лесные то сделать так что там густой лес… А движок можно поставить так что вдали деревья картинкой, когда подходиш ни преобразовываются в 3-хмерные деревья.
(...)

I'll try to translate it into English as close to the feel as possible.

Hello, I'm Kiril. I want you to make a game, 3D-aktion which is the following... The user may play with wood elves, palace guards and evil. And if the user plays elves, and if they are elfs, so they are in the forest, the wooden houses attuck palace soldiers and evils. Corovans can be robbed... And for the elf to make that there is a dense forest... The engine may be set up that further away the trees are pictures, and when you wolk thy trunsfarm into 3-mensional trees.
(...)

# The idea

So, yes, following Kiril's ideas here I try to convert trees into billboards (actually just quads) by automatically rendering them into texture, which really allows balancing FPS.

On the other hand such automatic rendering requires enormous amount of memory for textures.

So the final solution should be different and more complex. The current project demonstrates only one thing: how to render multiple objects into textures.

# The final goal

The final goal of these endeavours is creation of a huge Overworld, which would render at affordable FPS even on low-end graphic cards (like mine). This will most certainly require instancing and a lot of other tricks, but eventually I hope this will be possible. Thou the amount of work is extensive, and it will hardly ever appear as standalone example, but will rather be deeply integrated into Decoherence (the name will be changed soon) game https://gitlab.com/EugeneLoza/decoherence .