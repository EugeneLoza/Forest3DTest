unit CastleWorld;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fgl,
  CastleVectors,
  CastleScene, CastleSceneCore, x3dnodes,
  CastleSceneManager, CastleGlImages, CastleRectangles, CastleWindow,
  CastleCameras,
  CastleImages,
  CastleLog;

{$R+}{$Q+}

{ todo: support for different viewpoints simultaneous rendering,
  just needs to remember a rendered Texture for each camera/viewpoint }

type
  {Preforms render 3D to sprite and almost automatically switches
   sprite/3D object render based on external paramters}
  TCastleWorldObject = class(TComponent)
  public
    {copy of BaseScene.BoundingBox.Center}
    Center: TVector3;
    {normal 3D scene, can contain LODs and everything else}
    BaseScene: TCastleScene;
    {generated sprite scene contains just a quad with BaseScene rendered into it}
    SpriteScene: TCastleScene;
    {is this object currently displayed as a 3d or a sprite?}
    function IsSprite: boolean;
    {current cos(deviation angle) from last render
     TODO: Recalculated every time!}
    function Deviation_cos: single;
    {creates instance and initializes BaseScene and SpriteScene}
    constructor Create(AOwner: TCastleWindow); reintroduce;
    {switches to sprite}
    procedure SwitchSprite;
    {switches to 3d object}
    procedure SwitchBase;
    {render BaseScene to SpriteScene / copy of render_3d_to_texture_and_use_as_quad.CreateSpriteTexture

    BUG: WORKS ONLY ONCE PER APP START... WHY???}
    procedure RenderBaseToSprite;
    {copy Load parameters to BaseScene and initialize the sprite}
    procedure Load(ARootNode: TX3DRootNode; AOwnsRootNode: boolean;
      const AResetTime: boolean = True);
    {request this World Object to re-render}
    procedure RequestReRender;
  private
    {link to rendering camera}
    fCamera: TCamera;
    {container that renders the sprite}
    RenderContainer: TWindowContainer;
    {link to sprite texture for quick access}
    LinkToSprite: TCastleImage;
    {link to texture for quick access}
    LinkToTexture: TPixelTextureNode;
    {link to sprite transform node for quick access}
    LinkToTransform: TTransformNode;
    {Last camera position that rendered the sprite, determines if it needs to
     re-render}
    LastRenderLocation: TVector3;
    {almost a copy from render_3d_to_texture_and_use_as_quad.CreateRuntimeSceneNode}
    procedure InitSprite;
  public
    procedure TurnOn;
    procedure TurnOff;
  private
    fOn: boolean;
  end;

type
  TWorldList = specialize TFPGObjectList<TCastleWorldObject>;

{todo: fractal chunks! or at least 2 levels
 this should work as AbstractChunk...TListOfChunks...Chunk.Children: TListOfChunks}

type
  {this is a large World chunk containing a set of TCastleWorldObjects,
   it shouldn't be created by itself but instead is created
   and managed by TCastleWorld
   Chunks must be rectagonal to manage terrain heightmap unambigously}
  TCastleWorldChunk = class(TComponent)
  public
    {list of TCastleWorldObjects inside this chunk}
    Children: TWorldList;
    constructor Create(AOwner: TCastleWindow); reintroduce;
    destructor Destroy; override;
    {checks if location is inside chunkMin...chunkMax}
    function Inside(location: TVector3): boolean;
    {add a world object to this chunk}
    procedure Add(Obj: TCastleWorldObject);
    {switches to a representative sprite}
    procedure SwitchRepresentative;
    {switches to separate object}
    procedure SwitchChunk;
  private
    {this is a node created for RepresentativeObject,
     maybe it's not needed here and just a temporary node will do...
     but I'll leave it for now}
    RepresentativeNode: TX3DRootNode;
  public
    {if Representative is active and replaces Chunk's content}
    fRepresentative: boolean;
    {this is an object that represents the whole chunk. Used to
     render the whole chunk into a sprite}
    RepresentativeObject: TCastleWorldObject;
    {creates a representative node based on chunk's content}
    procedure MakeRepresentative;
  private
    {a container that renders this WorldChunk}
    //RenderContainer: TWindowContainer;
    fWindow: TCastleWindow;
  public
    ChunkMax, ChunkMin: TVector3;
    Center: TVector3;
  end;

{todo: maybe it'd be a good idea to re-chunk the world on the fly
 depending on current FPS? This is an efficiency-killer in an even
 terrain, but might be a life-saver in case of very inhomogeneous
 object density (i.e. passing from a meadow to forest)}

{UGLY STATIC ARRY. Needs to be fixed asap!!!}
const
  ChunksX = 6;
  ChunksY = 6;

type
  {This is a list of chunks.
  UGLY STATIC ARRAY. Needs to be fixed asap!!!}
  TChunkList_ugly = array [0..ChunksX, 0..ChunksY] of TCastleWorldChunk;

type
  TChunkList = specialize TFPGObjectList<TCastleWorldChunk>;

type
  {This is a global world manager that governs how chunks are generated
   and distributed. It also manages rendering pipeline}
  TCastleWorld = class(TComponent)
  private
  const
    {this is a goal which we try to achieve. Actually it must be variable
     and adjusted according to the world complexity, but at the moment
     we're using all the CPU resourses it can offer to get an unbelievable 100 FPS}
    GoalFPS = 1 / 50 * (1/24/60/60); {inverse}

    {these govern when sprite must be re-rendered.
     If deviation is as small (cosinus, actually) as MaxAccuracy
     then no re-render needed even in case CPU has spare time to}
    MaxAccuracy = 0.9999; {1 degree}
    {If deviation from last render position is less than MinAccuracy
     it must be re-rendered despite the FPS! }
    MinAccuracy = 0.996; {5 degrees}
  private
    {link to Window that governs this World}
    fWindow: TCastleWindow;
    {link to Window.SceneManager.Camera}
    fCamera: TCamera;
  private
    {these are the World Chunks.}
    WorldChunksUgly: TChunkList_ugly;
    WorldChunks: TChunkList;
    {this is a complete list of World objects in this World}
    WorldObjects: TWorldList;
  private
    WorldMax, WorldMin, WorldSize: TVector3Single;

  public
    {Window that governs render of this World Manager; read-only}
    property Window: TCastleWindow read fWindow;
    constructor Create(AOwner: TCastleWindow); reintroduce;
    destructor Destroy; override;
    procedure Add(Node: TX3DRootNode);
    //procedure Add(Scene: TCastleScene);
  public
    {time since start of the last render}
    RenderStart: TDateTime;
    {Current Distance where 3d objects turn to sprites}
    CurrentRenderDistance: single;
    {Current Distance where chunks turn to sprites}
    CurrentChunkDistance: single;
    {this is the global procedure that manages the world
     and tunes it to render/re-render based on Current FPS}
    procedure Manage;
    {non-chunk oriented management procedure. Let's keep it for history
     at the moment}
    procedure ManageObsolete; deprecated;
    {this procedure cuts the world evenly into chunks}
    procedure ChunkNSlice;
  end;

var
  Window: TCastleWindow;

implementation

{============================================================================}
{=========================== Castle world object =============================}
{============================================================================}


constructor TCastleWorldObject.Create(AOwner: TCastleWindow);
begin
  inherited Create(AOwner);
  RenderContainer := AOwner.container;   //maybe here goes the memory leak?
  fCamera := AOwner.SceneManager.Camera;
  BaseScene := TCastleScene.Create(AOwner);
  SpriteScene := TCastleScene.Create(AOwner);
end;

procedure TCastleWorldObject.InitSprite;
var
  Shape: TShapeNode;
  Appearance: TAppearanceNode;
  Coordinate: TCoordinateNode;
  QuadSet: TQuadSetNode;
  Texture: TPixelTextureNode;
  Transform: TTransformNode;
  Billboard: TBillboardNode;
  Root: TX3DRootNode;
begin
  Texture := TPixelTextureNode.Create;
  { clamp mode, not repeat --- avoids artifacts at the edges }
  Texture.RepeatS := False;
  Texture.RepeatT := False;
  LinkToTexture := Texture;

  RenderBaseToSprite;
  //BUG! Actually we don't need sprite now, but it will SIGSEGV unless we give it some image to start with

  Appearance := TAppearanceNode.Create;
  // You could assign some material to make it lit.
  // Appearance.Material := TMaterialNode.Create;
  Appearance.Texture := Texture;

  Coordinate := TCoordinateNode.Create;
  Coordinate.FdPoint.Items.AddArray(
    [Vector3(0, 0, 0), Vector3(BaseScene.BoundingBox.SizeX, 0, 0),
    Vector3(BaseScene.BoundingBox.SizeX, BaseScene.BoundingBox.SizeY, 0),
    Vector3(0, BaseScene.BoundingBox.SizeY, 0)]);

  QuadSet := TQuadSetNode.Create;
  QuadSet.FdCoord.Value := Coordinate;
  QuadSet.Solid := True; // see from both sides?

  { In real usecases, you could now assing explicit texture
    coordinates to QuadSet.FdTexCoord, instead of using the default
    calculated tex coordinates. }

  Shape := TShapeNode.Create;
  Shape.Appearance := Appearance;
  Shape.Geometry := QuadSet;

  Transform := TTransformNode.Create;
  Transform.Translation := vector3(
    BaseScene.BoundingBox.Center[0] - BaseScene.BoundingBox.sizex / 2,
    BaseScene.BoundingBox.Center[1] - BaseScene.BoundingBox.sizey / 2,
    BaseScene.BoundingBox.Center[2] - BaseScene.BoundingBox.sizez / 2);
  Transform.FdChildren.Add(Shape);
  LinkToTransform := Transform;

  {not working?}
  {Billboard := TBillboardNode.Create;
  billboard.AxisOfRotation := vector3single(0,1,0);
  billboard.FdChildren.Add(Transform); }

  Root := TX3DRootNode.Create;
  Root.FdChildren.Add(Transform);

  SpriteScene.Load(Root, True);
end;

procedure TCastleWorldObject.RequestReRender;
begin
  RenderBaseToSprite;
end;

procedure TCastleWorldObject.RenderBaseToSprite;
const
  TextureWidth = 16;
  TextureHeight = TextureWidth;
var
  SourceSceneManager: TCastleSceneManager;
  SourceCamera: TExamineCamera;
  RenderToTexture: TGLRenderToTexture;
  ViewportRect: TRectangle;
  popSceneVisibility: boolean;
begin
  Window.SceneManager.Items.Remove(BaseScene);

  {THIS PROCEDURE IS ~3 to 30 times slower than just direct render!}

  { nil everything first, to be able to wrap in a simple try..finally clause }
  SourceSceneManager := nil;
  RenderToTexture := nil;
  SourceCamera := nil;

  try
    SourceCamera := TExamineCamera.Create(nil);
    SourceCamera.SetView(fCamera.Position,
      Vector3(Center[0] - fCamera.Position[0],
      Center[1] - fCamera.Position[1],
      Center[2] - fCamera.Position[2]),
      fCamera.InitialUp, fCamera.GravityUp, True);
    //eeeemmmm??? Why do I get a SIGSEGV here if there are too little objects on the screen????? It has nothing to do with camera!
    LastRenderLocation := fCamera.Position;

    popSceneVisibility := BaseScene.Exists;
    BaseScene.Exists := True;

    SourceSceneManager := TCastleSceneManager.Create(nil);
    SourceSceneManager.Items.Add(BaseScene);
    SourceSceneManager.MainScene := BaseScene;
    SourceSceneManager.Camera := SourceCamera;

    RenderToTexture := TGLRenderToTexture.Create(TextureWidth, TextureHeight);
    RenderToTexture.Buffer := tbNone;
    RenderToTexture.GLContextOpen;


    RenderToTexture.RenderBegin;

    ViewportRect := Rectangle(0, 0, TextureWidth, TextureHeight);

    { Everything rendered between RenderBegin and RenderEnd is done off-screen.
      Below, we explicitly render the SourceSceneManager.
      This way, using this function doesn't change any Window.Controls
      or Window.SceneManager state, which is a nice thing. }
    RenderContainer.RenderControl(SourceSceneManager, ViewportRect);

    FreeAndNil(LinkToSprite);
    LinkToSprite := SaveScreen_NoFlush(TRGBAlphaImage, ViewportRect,
      RenderToTexture.ColorBuffer);
    LinkToTexture.FdImage.Value := LinkToSprite;
    //LinkToTexture.FdImage.Changed;
    //always empty/gray after first assignment????

    RenderToTexture.RenderEnd;

    BaseScene.Exists := popSceneVisibility;
  finally
    FreeAndNil(RenderToTexture);
    FreeAndNil(SourceSceneManager);
    FreeAndNil(SourceCamera);
  end;

  Window.SceneManager.Items.Add(BaseScene);
end;

function sqrdist(a, b: TVector3): single;
begin
  Result := sqr(a[0] - b[0]) + sqr(a[1] - b[1]) + sqr(a[2] - b[2]);
end;

function vecLen(a: TVector3): single;
begin
  Result := sqrt(sqr(a[0]) + sqr(a[1]) + sqr(a[2]));
end;

function diff(a, b: TVector3): TVector3Single;
begin
  Result[0] := a[0] - b[0];
  Result[1] := a[1] - b[1];
  Result[2] := a[2] - b[2];
end;

function mult(a, b: TVector3): single;
begin
  Result := a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
end;

function getCosinus(a, b, c: TVector3): single;
var
  v1, v2: TVector3;
begin
  v1 := diff(b, a);
  v2 := diff(c, a);
  Result := mult(v1, v2) / vecLen(v1) / vecLen(v2);
end;

function TCastleWorldObject.Deviation_cos: single;
begin
  Deviation_cos := abs(getCosinus(Center, fCamera.Position, LastRenderLocation));
end;

procedure TCastleWorldObject.SwitchSprite;
begin
  BaseScene.exists := False;
  SpriteScene.exists := True;
end;

procedure TCastleWorldObject.SwitchBase;
begin
  BaseScene.exists := True;
  SpriteScene.exists := False;
end;

function TCastleWorldObject.IsSprite: boolean;
begin
  Result := SpriteScene.exists;
end;

procedure TCastleWorldObject.Load(ARootNode: TX3DRootNode; AOwnsRootNode: boolean;
  const AResetTime: boolean = True);
begin
  BaseScene.Load(ARootNode, AOwnsRootNode, AResetTime);
  BaseScene.ProcessEvents := True;
  BaseScene.Spatial := [ssRendering, ssDynamicCollisions];
  Center := BaseScene.BoundingBox.Center;

  InitSprite;

  {doesn't change the FPS????}
  {SpriteScene.ProcessEvents := false;
  SpriteScene.Spatial := []; }
  SpriteScene.ProcessEvents := True;
  SpriteScene.Spatial := [ssRendering];
  SpriteScene.exists := False;

  TurnOn;
end;

procedure TCastleWorldObject.TurnOff;
begin
  fOn := False;
  SpriteScene.exists := False;
  BaseScene.exists := False;
end;

procedure TCastleWorldObject.TurnOn;
begin
  fOn := True;
end;

{============================================================================}
{=========================== Castle world chunk =============================}
{============================================================================}

constructor TCastleWorldChunk.Create(AOwner: TCastleWindow);
begin
  inherited Create(AOwner);
  fWindow := AOwner;
  Children := TWorldList.Create(False); //this list is non exclusive
end;

destructor TCastleWorldChunk.Destroy;
begin
  FreeAndNil(Children);
  inherited;
end;

function TCastleWorldChunk.Inside(Location: TVector3): boolean;
begin
  if (Location[0] >= chunkMin[0]) and (Location[0] <= chunkMax[0]) and
    (Location[1] >= chunkMin[1]) and (Location[1] <= chunkMax[1]) and
    (Location[2] >= chunkMin[2]) and (Location[2] <= chunkMax[2]) then
    Result := True
  else
    Result := False;
end;

procedure TCastleWorldChunk.Add(Obj: TCastleWorldObject);
begin
  Children.Add(Obj);
end;

procedure TCastleWorldChunk.MakeRepresentative;
var
  i, j: integer;
begin
  {todo: simplify the Node and use the lowest LODs if available}
  {todo: actually RepresentativeNode may and must contain sprites! to speed up things}

  {here we scan all the chunk's Children and extract their Children here}
  RepresentativeNode := TX3DRootNode.Create;
  for i := 0 to self.Children.Count - 1 do
    //RepresentativeNode.fdChildren.Add(Children[i].BaseScene.RootNode);
    for j := 0 to Children[i].BaseScene.RootNode.FdChildren.Count - 1 do
      RepresentativeNode.fdChildren.Add(Children[i].BaseScene.RootNode.FdChildren[j]);

  WriteLnLog('TCastleWorldChunk.MakeRepresentative', 'nodes = ' + IntToStr(
    RepresentativeNode.fdChildren.Count));
  if RepresentativeNode.fdChildren.Count > 0 then
  begin
    RepresentativeObject := TCastleWorldObject.Create(self.fWindow);
    RepresentativeObject.Load(RepresentativeNode, True);
  end;
end;

procedure TCastleWorldChunk.SwitchRepresentative;
var
  i: integer;
begin
  if fRepresentative = False then
  begin
    fRepresentative := True;
    for i := 0 to Children.Count - 1 do
      Children[i].TurnOff;
    RepresentativeObject.turnOn;
    RepresentativeObject.BaseScene.Exists := False;  //never turned on
    RepresentativeObject.SpriteScene.Exists := True;
  end;
end;

procedure TCastleWorldChunk.SwitchChunk;
var
  i: integer;
begin
  if fRepresentative = True then
  begin
    fRepresentative := False;
    for i := 0 to Children.Count - 1 do
      Children[i].TurnOn;
    RepresentativeObject.turnOff;
  end;
end;


{============================================================================}
{================================ Castle world ==============================}
{============================================================================}

constructor TCastleWorld.Create(AOwner: TCastleWIndow);
begin
  inherited Create(AOwner);
  fWindow := AOwner;
  fCamera := fWindow.SceneManager.Camera;
  WorldObjects := TWorldList.Create(False);
  //?this is a owner's list of all objects in the World

  RenderStart := -1;
end;

destructor TCastleWorld.Destroy;
begin
  FreeAndNil(WorldObjects);
  FreeAndNil(WorldChunks);
  inherited;
end;

procedure TCastleWorld.Add(Node: TX3DRootNode);
var
  NewObject: TCastleWorldObject;
begin
  NewObject := TCastleWorldObject.Create(Self.Window);
  NewObject.load(Node, True);
  WorldObjects.Add(NewObject);
  //Window.scenemanager.Items.Add(NewObject.BaseScene);
  Window.scenemanager.Items.Add(NewObject.SpriteScene);
end;

procedure TCastleWorld.Manage;
var
  i, j: integer;
  Distance: single;
  MostWanted: integer;
  SoftList: TWorldList;
begin
  if RenderStart < 0 then
    RenderStart := now;
  //we're first render, just not to get an "incorrectly initialized" variable

  SoftList := TworldList.Create(False);

  CurrentChunkDistance := 2 * CurrentRenderDistance; //todo
  for j := 0 to WorldChunks.Count - 1 do
  begin
    Distance := sqr(WorldChunks[j].Center[0] - fCamera.Position[0]) +
      sqr(WorldChunks[j].Center[1] - fCamera.Position[1]) +
      sqr(WorldChunks[j].Center[2] - fCamera.Position[2]);
    if Distance > CurrentChunkDistance then
      WorldChunks[j].SwitchRepresentative
    else
    begin
      WorldChunks[j].SwitchChunk;
      for i := 0 to WorldChunks[j].Children.Count - 1 do
      begin
        Distance := sqr(WorldChunks[j].Children[i].Center[0] - fCamera.Position[0]) +
          sqr(WorldChunks[j].Children[i].Center[1] - fCamera.Position[1]) +
          sqr(WorldChunks[j].Children[i].Center[2] - fCamera.Position[2]);
        if Distance > CurrentRenderDistance then
        begin
          WorldChunks[j].Children[i].switchSprite;
          {add this sprite to refresh lists}
          if WorldChunks[j].Children[i].Deviation_cos < MaxAccuracy then
          begin
            if WorldChunks[j].Children[i].Deviation_cos < MinAccuracy then
              WorldChunks[j].Children[i].RequestReRender
            //re-render these sprites at once!
            else
              SoftList.Add(WorldChunks[j].Children[i]);
          end;
        end
        else
          WorldChunks[j].Children[i].switchBase;
      end;
    end;
  end;

  {now we cycle through all sprites that might softly need re-render}
  if ((now - RenderStart) > GoalFPS) and (SoftList.Count > 0) then
  begin
    //SoftList.Sort(how?);
    while (SoftList.Count > 0) and ((now - RenderStart) < GoalFPS) do
    begin
      MostWanted := 0;
      for i := 1 to SoftList.Count - 1 do
        if (SoftList[i].Deviation_cos < SoftList[MostWanted].Deviation_cos) then
          MostWanted := i;
      if MostWanted >= 0 then
        WorldObjects[MostWanted].RequestReRender;
    end;
  end;
  FreeAndNil(SoftList);

  //FPS.RealTime is too laggy for such fine-tuning...
  {THIS WORKS NOT CORRECTLY, it can go down to zero or to infinity.
   Should be governed better, but this is a prof-of-concept, still lots to be done}
  if now - RenderStart < GoalFPS then
    CurrentRenderDistance := CurrentRenderDistance * 1.01
  else
    CurrentRenderDistance := CurrentRenderDistance * 0.99;
  {if now-RenderStart>0 then
    CurrentRenderDistance := CurrentRenderDistance*(GoalFPS/(now-RenderStart));}
  //todo: min and max Distance

  {here we need this value to contain "total" frame time =
   render + sprites manipulations}
  RenderStart := now;
end;

procedure TCastleWorld.ManageObsolete;
var
  i: integer;
  Distance: single;
  MostWanted: integer;
begin
  if RenderStart < 0 then
    RenderStart := now;
  //we're first render, just not to get an "incorrectly initialized" variable

  for i := 0 to WorldObjects.Count - 1 do
  begin
    { globalSceneList[i].BoundingBox.IsEmpty is always true in case exists = false !
     I'm not sure how to make that automatic... I just had to make temporary
     if true{GetExists} then ... in TCastleSceneCore.BoundingBox
     maybe some "last seen" bounding box or something like it can be made?}
    if not WorldObjects[i].BaseScene.BoundingBox.IsEmpty then
      Distance := sqr(WorldObjects[i].Center[0] - fCamera.Position[0]) +
        sqr(WorldObjects[i].Center[1] - fCamera.Position[1]) +
        sqr(WorldObjects[i].Center[2] - fCamera.Position[2])
    else
      Distance := 0;

    if Distance > CurrentRenderDistance then
      WorldObjects[i].switchSprite
    else
      WorldObjects[i].switchBase;
  end;

  {now we cycle through all sprites}
  repeat
    MostWanted := -1;
    //todo: make it a sorted list!
    for i := 0 to WorldObjects.Count - 1 do
      if WorldObjects[i].isSprite then
      begin
        if WorldObjects[i].Deviation_cos < MinAccuracy then
          WorldObjects[i].RequestReRender
        else
        if (MostWanted < 0) or (WorldObjects[i].Deviation_cos <
          WorldObjects[MostWanted].Deviation_cos) then
          MostWanted := i;
      end;
    if MostWanted > 0 then
      WorldObjects[MostWanted].RequestReRender;
  until (MostWanted < 0) or (WorldObjects[MostWanted].Deviation_cos > MaxAccuracy) or
    ((now - RenderStart) > GoalFPS * 0.9);

  //FPS.RealTime is too laggy for such fine-tuning...
  {THIS WORKS NOT CORRECTLY, it can go down to zero or to infinity.
   Should be governed better, but this is a prof-of-concept, still lots to be done}
  if now - RenderStart < GoalFPS then
    CurrentRenderDistance := CurrentRenderDistance * 1.001
  else
    CurrentRenderDistance := CurrentRenderDistance * 0.999;
  {if now-RenderStart>0 then
    CurrentRenderDistance := CurrentRenderDistance*(GoalFPS/(now-RenderStart));}
  //todo: min and max Distance

  {here we need this value to contain "total" frame time =
   render + sprites manipulations}
  RenderStart := now;
end;

procedure TCastleWorld.ChunkNSlice;
var
  i: integer;
  ix, iy: integer;
  found: boolean;
begin
  {calculate world size. Actually it's size between Centers of the
   World Objects. So the actual world is a little bigger}
  //WorldObjects.Count>1!
  WorldMax[0] := WorldObjects[0].Center[0];
  WorldMax[1] := WorldObjects[0].Center[1];
  WorldMax[2] := WorldObjects[0].Center[2];
  WorldMin[0] := WorldObjects[0].Center[0];
  WorldMin[1] := WorldObjects[0].Center[1];
  WorldMin[2] := WorldObjects[0].Center[2];
  for i := 1 to WorldObjects.Count - 1 do
  begin
    if WorldMax[0] < WorldObjects[i].Center[0] then
      WorldMax[0] := WorldObjects[i].Center[0];
    if WorldMax[1] < WorldObjects[i].Center[1] then
      WorldMax[1] := WorldObjects[i].Center[1];
    if WorldMax[2] < WorldObjects[i].Center[2] then
      WorldMax[2] := WorldObjects[i].Center[2];
    if WorldMin[0] > WorldObjects[i].Center[0] then
      WorldMin[0] := WorldObjects[i].Center[0];
    if WorldMin[1] > WorldObjects[i].Center[1] then
      WorldMin[1] := WorldObjects[i].Center[1];
    if WorldMin[2] > WorldObjects[i].Center[2] then
      WorldMin[2] := WorldObjects[i].Center[2];
  end;
  WorldSize[0] := WorldMax[0] - WorldMin[0];
  WorldSize[1] := WorldMax[1] - WorldMin[1];
  WorldSize[2] := WorldMax[2] - WorldMin[2];

  {todo: Chunk'n'slice expects Y-up orientation.
   It must work with gravityUp! Because I prefer Z-up :D
   But this is just a proof-of-concept for now...}
  {prepare chunks to accept data}
  for ix := 0 to ChunksX do
    for iy := 0 to ChunksY do
    begin
      WorldChunksUgly[ix, iy] := TCastleWorldChunk.Create(Window);
      with WorldChunksUgly[ix, iy] do
      begin
        ChunkMin[0] := WorldMin[0] + ix * WorldSize[0] / (ChunksX + 1);
        ChunkMin[1] := WorldMin[1];
        ChunkMin[2] := WorldMin[2] + iy * WorldSize[2] / (ChunksY + 1);
        ChunkMax[0] := WorldMin[0] + (ix + 1) * WorldSize[0] / (ChunksX + 1);
        ChunkMax[1] := WorldMax[1];
        ChunkMax[2] := WorldMin[2] + (iy + 1) * WorldSize[2] / (ChunksY + 1);

        Center[0] := (chunkMax[0] + ChunkMin[0]) / 2;
        Center[1] := (chunkMax[1] + ChunkMin[1]) / 2;
        Center[2] := (chunkMax[2] + ChunkMin[2]) / 2;
      end;
    end;

  {provide data for chunks}
  for i := 0 to WorldObjects.Count - 1 do
  begin
    found := False;
    for ix := 0 to ChunksX do
    begin
      for iy := 0 to ChunksY do
        if WorldChunksUgly[ix, iy].inside(WorldObjects[i].Center) then
        begin
          WorldChunksUgly[ix, iy].Add(WorldObjects[i]);
          found := True;
          break;
        end;
      if found then
        break;
    end;
  end;

  {make a all-chunk representative node}
  for ix := 0 to ChunksX do
    for iy := 0 to ChunksY do
      WorldChunksUgly[ix, iy].MakeRepresentative;

  WorldChunks := TChunkList.Create(False);
  for ix := 0 to ChunksX do
    for iy := 0 to ChunksY do
      WorldChunks.Add(WorldChunksUgly[ix, iy]);

  for ix := 0 to ChunksX do
    for iy := 0 to ChunksY do
      Window.SceneManager.Items.Add(WorldChunksUgly[ix, iy].RepresentativeObject.SpriteScene);


  CurrentRenderDistance := 10000; //todo: should be generated based on WorldSize;
  //or better: a sorted list should be created and it's on/off position should shift
  CurrentChunkDistance := 2 * CurrentRenderDistance;
end;


end.
